This project was used to show how to configure and send an email notification with User Notification Service (UMS) in OSB. 

In detail it shows how to configure the following properties in a OSB pipeline:
* the subject
* the receiver of the email
* the sender of the email
* the message body
